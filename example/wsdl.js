'use strict';

const path = require('path');
const util = require('util');
var soap = require('..').soap;
// wsdl of the web service this client is going to invoke. For local wsdl you can use, url = './wsdls/stockquote.wsdl'
var url = path.join(__dirname, './wsdls/Learning.wsdl');

var requestArgs = {
  symbol: 'IBM',
};

var options = {};
soap.createClient(url, options, function(err, client) {
  if (err) console.error(err);
  else {
    console.log(
      util.inspect(
        client.describe().LearningService.Learning.Manage_Learning_Course.input
          .body.elements[0],
        {depth: 10}
      )
    );
  }
});
